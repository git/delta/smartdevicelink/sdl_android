# 5.6.1 Release Notes

## Summary:

|| Version|
|---|---|
| **Protocol** | 5.4.1 |
| **RPC** | 8.0.0 |
| **Tested Targeting** | Android 33 |

## Bug Fix / Enhancements:

- [Nearby Devices permission on Android 12 and Above #1839](https://github.com/smartdevicelink/sdl_java_suite/issues/1839)